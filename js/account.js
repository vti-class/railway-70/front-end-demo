// Hàm mở đầu: Khi load trang sẽ chạy các function ở đây
$(function () {
    getListAccount();
});

let accountList = [];
let apiBaseAccount = "https://63a073aae3113e5a5c3e0675.mockapi.io/User"

function getListAccount() {
    // call api -> lấy ra danh sách account
    // gán gía trị vừa lấy được cho accountList
    $.ajax({
        url: apiBaseAccount,
        type: 'GET',
        contentType: 'application/json', // Định nghĩa định dạng dữ liệu truyền vào là json
        // data: JSON.stringify(request),
        error: function (err) {
            // Hành động khi apii bị lỗi
            console.log(err)
            alert(err.responseJSON);
        },
        success: function (data) {
            // Hành động khi thành công
            fillDataToTable(data);
        }
    });
}

function fillDataToTable(data) {
    console.log(data)
    accountList = data;
    $('tbody').empty();
    accountList.forEach(function (item, index) {
        $('tbody').append(
            `<tr>
            <th scope="row" class="align-middle">`+ (index + 1) + `</th>
            <td class="align-middle">
                <img src="`+ item.avatar + `">
            </td>
            <td class="align-middle">`+ item.name + `</td>
            <td class="align-middle">`+ item.address + `</td>
            <td class="align-middle">`+ item.createdAt + `</td>
            <td class="align-middle">
                <i class="fa fa-pencil text-warning m-1" style="font-size: 24px;" onclick="openUpdateAccountModal(`+ item.id + `)"></i>
                <i class="fa fa-trash text-danger m-1" style="font-size: 24px;" onclick="openModalDelete(`+ item.id + `)"></i>
            </td>
          </tr>`
        )
    });
}

function openUpdateAccountModal(accountId) {
    document.getElementById("accountModalTitle").innerText = "Update Account";
    $('#modalAccount').modal('show');
    // Lấy được đối tượng Account mà muốn update
    // C1: Sử dụng accountList -> tìm ra account có id là accountId
    let account = accountList.find((item) => {
        if (item.id == accountId) {
            return item;
        }
    })
    console.log(account)
    // Gán giá trị id -> inputID,... trong modal
    document.getElementById("accountIdUpdate").value = accountId;
    document.getElementById("username").value = account.name;
    document.getElementById("avatar").value = account.avatar;
    document.getElementById("address").value = account.address;

    // C2: Call API get by ID
}

function openModalDelete(accountId) {
    if (confirm("Bạn muốn xoá account có ID là: " + accountId)) {
        alert("Bạn đã xoá thành công")
    }
}

function openAddAccountModal() {
    document.getElementById("accountModalTitle").innerText = "Thêm mới Account";
    // Reset giá trị accountID,... trong modal
    document.getElementById("accountIdUpdate").value = "";
    document.getElementById("username").value = "";
    document.getElementById("avatar").value = "";
    document.getElementById("password").value = "";
    document.getElementById("address").value = "";
}

function saveAccount() {
    let accountID = document.getElementById("accountIdUpdate").value;
    if (accountID) {
        onUpdateAccount();
    } else {
        // Chức năng thêm mới
        onAddAccount();
    }
}

function Account(username, avatar, address, createDate) {
    this.name = username;
    this.avatar = avatar;
    this.address = address;
    this.createDate = createDate;
}

function onAddAccount() {
    let username = document.getElementById("username").value
    let avatar = document.getElementById("avatar").value
    let address = document.getElementById("address").value
    let createDate = Date.now();
    // Call API -> Thêm mới account
    // Call API
    // Cách 1: Tạo 1 object trực tiếp
    let account = { "name": username, "avatar": avatar, "address": address, "createDate": createDate }
    // Cách 2: Dùng constructor
    let account2 = new Account(username, avatar, address, createDate);

    $.ajax({
        url: apiBaseAccount,
        type: "POST",
        contentType: 'application/json', // Định nghĩa định dạng dữ liệu truyền vào là json
        data: JSON.stringify(account), // Đối tượng account
        error: function (err) {
            // Hành động khi apii bị lỗi
            console.log(err)
            alert(err.responseJSON);
        },
        success: function (data) {
            // Hành động khi thành công
            getListAccount();// Lấy lại danh sách account
            $('#modalAccount').modal('hide'); // Đóng modal thêm mới/update
            showAlrtSuccess("Thêm mới account thành công"); // Hiển thị Aleart thông báo đã thêm mới thành công
        }
    });
}

function onUpdateAccount() {
    let accountId = document.getElementById("accountIdUpdate").value
    let username = document.getElementById("username").value
    let avatar = document.getElementById("avatar").value
    let address = document.getElementById("address").value
    let createDate = Date.now();
    // Call API -> Update account
    // Call API
    // Cách 1: Tạo 1 object trực tiếp
    let account = {"name": username, "avatar": avatar, "address": address, "createDate": createDate }

    $.ajax({
        url: apiBaseAccount + "/" + accountId,
        type: "PUT",
        contentType: 'application/json', // Định nghĩa định dạng dữ liệu truyền vào là json
        data: JSON.stringify(account), // Đối tượng account
        error: function (err) {
            // Hành động khi apii bị lỗi
            console.log(err)
            alert(err.responseJSON);
        },
        success: function (data) {
            // Hành động khi thành công
            getListAccount();// Lấy lại danh sách account
            $('#modalAccount').modal('hide'); // Đóng modal thêm mới/update
            showAlrtSuccess("Update account thành công"); // Hiển thị Aleart thông báo đã thêm mới thành công
        }
    });
}

// Tạo 1 hàm chung để thông báo thành công
function showAlrtSuccess(text) {
    document.getElementById("text-modal-success").innerText = text;
    $("#modal-success").fadeTo(2000, 500).slideUp(500, function () {
        $("#modal-success").slideUp(3000);
    });
}