// Hàm mở đầu: Khi load trang sẽ chạy các function ở đây
$(function () {
// $("#header") ~ document.getElementById("header")
    $("#header").load("./html/header.html");
    $("#body").load("./html/account.html");
    $("#footer").load("./html/footer.html");
    // getListAccount();
});

function clickNavihome(){
    $("#body").load("./html/home.html");
}

function clickNaviViewListAccount(){
    $("#body").load("./html/account.html");
}

function clickNaviViewListDepartment(){
    $("#body").load("./html/department.html");
}

function logout(){
    if(confirm("Bạn muốn đăng xuất?")){
        alert("Bạn đã đăng xuất!")
    }
}